// Set Global Variables
const recipeFolder = "_recipes";

readRecipeList();
const listImageHeight = "72px";

/* Only register a service worker if it's supported */
// ServiceWorker is a progressive technology. Ignore unsupported browsers
if ('serviceWorker' in navigator) {
    console.log('CLIENT: service worker registration in progress.');
        navigator.serviceWorker.register('/service-worker.js').then(function() {
            console.log('CLIENT: service worker registration complete.');
        }, function() {
            console.log('CLIENT: service worker registration failure.');
    });
    } else {
        console.log('CLIENT: service worker is not supported.');
}

// List

function readRecipeList() {
    var myFile = new XMLHttpRequest();
    myFile.open("GET", `${recipeFolder}/index.txt` ,true);
    myFile.send();

    myFile.onreadystatechange = function() {
        if (myFile.readyState== 4 && myFile.status == 200) {
            rawcontent = myFile.responseText;
            displayRecipeList();
        }
    }
}

function displayRecipeList() {
    var content = rawcontent.split("\n");
    var output = "";

    for (let i = 0; i < content.length; i++) {
        output += `<div class="card horizontal">`
            output += `<div class="card-image">`
                output += `<img src="${recipeFolder}/${content[i]}/image.jpg" style="width:300px;height:100%;object-fit:cover">`
            output += `</div>`
            output += `<div class="card-stacked">`
                output += `<div class="card-content">`
                    output += `<h3 style="margin-top:0px;text-transform: capitalize;">${content[i].replace('-', ' ')}</h3>`
                    output += `<p id="text${content[i]}">I am a very simple card. I am good at containing small bits of information.</p>`
                output += `</div>`
                output += `<div class="card-action">`
                    output += `<a href="javascript:openRecipe('${content[i]}')">Open</a>`
                output += `</div>`
            output += `</div>`
        output += `</div>`
        readDescription(content[i]);
    }

    document.getElementById("list").innerHTML = output;
}

function readDescription(recipe) {
    var descriptionFile = new XMLHttpRequest();
    descriptionFile.open("GET", `${recipeFolder}/${recipe}/details.txt` ,true);
    descriptionFile.send();
    descriptionFile.onreadystatechange = function() {
        if (descriptionFile.readyState== 4 && descriptionFile.status == 200) {
            document.getElementById("text" + recipe).innerHTML = descriptionFile.responseText;
        }
    }
}


// Recipe

function openRecipe(recipe) {
    document.getElementById("recipe").style.visibility = "visible";
    document.getElementById("recipeTitle").innerHTML = recipe;

    // Description
    var descriptionFile = new XMLHttpRequest();
    descriptionFile.open("GET", `${recipeFolder}/${recipe}/details.txt` ,true);
    descriptionFile.send();
    descriptionFile.onreadystatechange = function() {
        if (descriptionFile.readyState== 4 && descriptionFile.status == 200) {
            document.getElementById("recipeDescription").innerHTML = descriptionFile.responseText;
        }
    }

    // Ingredients
    var ingredientFile = new XMLHttpRequest();
    ingredientFile.open("GET", `${recipeFolder}/${recipe}/ingredients.txt` ,true);
    ingredientFile.send();
    ingredientFile.onreadystatechange = function() {
        if (ingredientFile.readyState== 4 && ingredientFile.status == 200) {
            document.getElementById("recipeIngredients").innerHTML = fillIngredients(recipe, ingredientFile.responseText);
        }
    }

    // Ingredients
    var directionFile = new XMLHttpRequest();
    directionFile.open("GET", `${recipeFolder}/${recipe}/directions.txt` ,true);
    directionFile.send();
    directionFile.onreadystatechange = function() {
        if (directionFile.readyState== 4 && directionFile.status == 200) {
            document.getElementById("recipeDirections").innerHTML = fillDirections(recipe, directionFile.responseText);
        }
    }

    document.getElementById('recipe').scrollIntoView();
}

function fillIngredients(recipe, ingredients) {
    ingredients = ingredients.split("\n");

    output = "";
    for (let i = 0; i < ingredients.length; i++) {
        output += "<p><label>";
        output += `<input type="checkbox"><span>${ingredients[i]}</span>`
        output += "</label></p>";
    }
    return output;
}

function fillDirections(recipe, ingredients) {
    ingredients = ingredients.split("\n");

    output = "";
    for (let i = 0; i < ingredients.length; i++) {
        output += "<p><label>";
        output += `<input type="checkbox"><span>${ingredients[i]}</span>`
        output += "</label></p>";
    }
    return output;
}